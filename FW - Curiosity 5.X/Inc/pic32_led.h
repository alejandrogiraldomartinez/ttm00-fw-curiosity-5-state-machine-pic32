/* ************************************************************************** */
/** Descriptive File Name
 
 @Company
 Titoma Design Ltd.
 
 @File Name
 leds_pic32.h
 
 @Summary
 Brief description of the file.
 
 @Description
 Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "plib.h"
#include <stdint.h>
#include <stdio.h>

typedef enum _led_select_{
    LED_invalid,
            LED_1,
            LED_2,
            LED_3,
            LED_R,
            LED_G,
            LED_B
}led_select_t;

void LED_on ( uint8_t LED_X ) ;
void LED_off ( uint8_t LED_X ) ;
void LED_toggle ( uint8_t LED_X ) ;
void LED_alternate ( uint8_t LED_X ) ;
void LED_error_handler();