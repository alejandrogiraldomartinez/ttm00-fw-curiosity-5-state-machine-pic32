
#include "../Inc/pic32_led.h"


/**
 * @brief    Switch-case to turn on <LED_X> (LED_1, LED_2, LED_3)
 * @param    <LED_X> component of the enum indicating one of the 3 leds 
 * @retval void
 */
void LED_on ( uint8_t LED_X ) {
  if ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 || LED_X == LED_R || LED_X == LED_G || LED_X == LED_B ) {
    switch ( LED_X ) {
      case LED_1:
        mPORTESetBits ( BIT_4 ) ;
        break;         
      case LED_2:
        mPORTESetBits ( BIT_6 ) ;
        break;           
      case LED_3:
        mPORTESetBits ( BIT_7 ) ;
        break;
      case LED_R:
        mPORTBClearBits ( BIT_10 ) ;
        break;         
      case LED_G:
        mPORTBClearBits ( BIT_3 ) ;
        break;            
      case LED_B:
        mPORTBClearBits ( BIT_2 ) ;
        break;          
      default:
        break;
    }//switch ( LED_X )
  }//  if ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 )
  else {
    LED_error_handler();
  }
}//void LED_on 

/**
 * @brief    Switch-case to turn off <LED_X> (LED_1, LED_2, LED_3)
 * @param    <LED_X> component of the enum indicating one of the 3 leds 
 * @retval void
 */
void LED_off ( uint8_t LED_X ) {
  if ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 || LED_X == LED_R || LED_X == LED_G || LED_X == LED_B ) {
    switch ( LED_X ) {
      case LED_1: 
        mPORTEClearBits ( BIT_4 ) ;
        break;
      case LED_2: 
        mPORTEClearBits ( BIT_6 ) ;
        break;
      case LED_3: 
        mPORTEClearBits ( BIT_7 ) ;
        break;
      case LED_R:
        mPORTBSetBits ( BIT_10 ) ;
        break;        
      case LED_G:
        mPORTBSetBits ( BIT_3 ) ;
        break;        
      case LED_B:
        mPORTBSetBits ( BIT_2 ) ;
        break;   
      default:
        break;
    } //switch ( LED_X ) 
  }//if ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 )
  else {
    LED_error_handler();
  }
}//void LED_off

/**
 * @brief    Switch-case to toggle <LED_X> (LED_1, LED_2, LED_3)
 * @param    <LED_X> component of the enum indicating one of the 3 leds 
 * @retval void
 */
void LED_toggle ( uint8_t LED_X ) {
  if ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 || LED_X == LED_R || LED_X == LED_G || LED_X == LED_B ) {
    switch ( LED_X ) {
      case LED_1: 
        mPORTEToggleBits ( BIT_4 ) ;
        break;           
      case LED_2:
        mPORTEToggleBits ( BIT_6 ) ;
        break;     
      case LED_3:
        mPORTEToggleBits ( BIT_7 ) ;
        break;             
      case LED_R:
        mPORTBToggleBits ( BIT_10 ) ;
        break;             
      case LED_G:
        mPORTBToggleBits ( BIT_3 ) ;
        break;              
      case LED_B:
        mPORTBToggleBits ( BIT_2 ) ;
        break;             
      default:
        break;
    } //Switch ( LED_X )
  }//if  ( LED_X == LED_1 || LED_X == LED_2 || LED_X == LED_3 )
  else {
    LED_error_handler();
  }
}//void LED_toggle 

/**
 * @brief    Switch-case to alternate  <LED_X> (LED_1, LED_2, LED_3)
 * @param    <LED_X> component of the enum indicating one of the 3 leds 
 * @retval void
 */
void LED_alternate ( uint8_t LED_X ) {
  switch ( LED_X ) {
    case LED_1:
      mPORTESetBits ( BIT_4 );
      mPORTEClearBits ( BIT_6 | BIT_7 ) ;
      break;            
    case LED_2:
      mPORTESetBits ( BIT_6 ) ;
      mPORTEClearBits ( BIT_4 | BIT_7 ) ;
      break;
    case LED_3:
      mPORTESetBits ( BIT_7 ) ;
      mPORTEClearBits ( BIT_6 | BIT_4 ) ;
      break;            
    default:
      break;
  }
}

void LED_error_handler(){
  printf("Error detected, halting");
  while(1) {};
}